# Centrality

It is necessary to have installed:
1) Python 3.7
2) PIP (package manager)
3) node 12.18 + 
4) npm 6.14 + 

## Installation
pip install -r requirements.txt

-> front: npm i

## Run Backend
python run.py

## Run UI
-> front: npm start

You can see the result here - http://localhost:3000


