import axios from "axios";

export default class ApiService {

    apiBase = 'http://localhost:5000/';
    headers = { 'Content-Type': 'application/x-www-form-urlencoded', "Access-Control-Allow-Origin": "*" };
    apiLogin = "login";
    apiRegister = "register";
    apiLogout = "logout";
    apiGetUserData = "user/";
    apiUpdateUserData = "update-user";
    apiCentralityTable = "get-centralities";
    apiCentrality = "get-centrality/";
    apiUpdateCentrality = "update-centrality";
    apiDeleteCentrality = "delete-centrality";
    apiCreateCentrality = "create-centrality";

    login = async (isLogin, data) => {
        return await axios.post(this.apiBase + (isLogin ? this.apiLogin : this.apiRegister), {
            method: "post",
            data: data,
            headers: this.headers
        })
    };

    logout = async() => {
        return await axios.get(this.apiBase + this.apiLogout, {
            headers: this.headers
        })
    }

    getUserData = async(key) => {
        return await axios.get(this.apiBase + this.apiGetUserData + key, {
            headers: this.headers
        })
    }

    updateUserData = async(data) => {
        return await axios.post(this.apiBase + this.apiUpdateUserData, {
            method: "post",
            data: data,
            headers: this.headers
        })
    }

    getCentralityTable = async() => {
        return await axios.get(this.apiBase + this.apiCentralityTable, {
            headers: this.headers
        })
    }

    getCentralityById = async(id) => {
        return await axios.get(this.apiBase + this.apiCentrality + id, {
            headers: this.headers
        })
    }

    updateCentrality = async(data, isNew) => {
        return await axios.post(this.apiBase + (isNew ? this.apiCreateCentrality : this.apiUpdateCentrality), {
            method: "post",
            data: data,
            headers: this.headers
        })
    }

    deleteCentrality = async(data) => {
        return await axios.post(this.apiBase + this.apiDeleteCentrality, {
            method: "post",
            data: data,
            headers: this.headers
        })
    }

    createCentrality = async(data) => {
        return await axios.post(this.apiBase + this.apiCreateCentrality, {
            method: "post",
            data: data,
            headers: this.headers
        })
    }

}