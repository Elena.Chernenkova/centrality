import React from "react";

import "antd/lib/radio/style/css";
import "antd/lib/button/style/css";
import "antd/lib/input/style/css";
import "antd/lib/form/style/css";
import "antd/lib/notification/style/css";

import { useHistory } from 'react-router-dom'


import "./LoginPage.scss";
import { Form, Input, Button, Radio, notification } from 'antd';
import ApiService from "../../services/api";

const apiService = new ApiService();

const openNotification = (text: string) => {
    notification.error({
        message: "Не удается войти",
        description: text
    });
};

const LoginPage = (props: any) => {
    const [mode, setMode] = React.useState('enter');
    const history = useHistory();

    const onSend = (values: any) => {
        apiService.login(mode === 'enter', values).then(function (response) {
            if (response.data.success === "true") {
                props.setLogged({logged: true, id: response.data.id});
                history.push('/');
            } else {
                openNotification(response.data.error);
            }
        })
    };

    const toggleMode = (target: any) => {
        setMode(target.target.value);
    };

    const options = [
        { label: 'Вход', value: 'enter' },
        { label: 'Регистрация', value: 'register' },
    ];

    return (
        <div className="login-page">
            <div style={{
                paddingBottom: '10%'
            }}>
                <Radio.Group
                    options={options}
                    onChange={toggleMode}
                    value={mode}
                    optionType="button"
                    buttonStyle="solid"
                />
            </div>
            <Form
                layout='vertical'
                name="basic"
                onFinish={onSend}
            >
                <Form.Item
                    label="Логин"
                    name="user"
                    rules={[{required: true, message: 'Введите более 3 и более символов', min: 3}]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Пароль"
                    name="password"
                    rules={[{required: true, message: 'Введите более 3 и более символов', min: 3}]}
                >
                    <Input.Password/>
                </Form.Item>


                <Form.Item style={{
                    paddingTop: '7%'
                }}>
                    <Button type="primary" htmlType="submit">
                        Продолжить
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
}

export default LoginPage;
