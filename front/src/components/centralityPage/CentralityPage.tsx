import React from 'react';

import {Button, Form, Input, notification, Spin} from 'antd';

import "./CentralityPage.scss";
import ApiService from "../../services/api";
import { useHistory } from "react-router-dom";

const apiService = new ApiService();


interface CentralityI {
    id?: number;
    name?: string;
    year?: string;
    description?: string;
    link?: string;
    codeLink?: string;
}

export const CentralityPage = (props: any) => {
    const [data, setData] = React.useState<CentralityI>({});
    const history = useHistory();
    const id = props.match.params.id;


    React.useEffect(() => {
        if (id !== "new") {
            apiService.getCentralityById(id).then((resp) => {
                setData(resp.data.data)
            })
        }
    }, [])

    const updateCentrality = (values: any) => {
        apiService.updateCentrality({...values, id: data.id}, id === "new").then((resp) => {
            if (resp.data.success === "true") {
                addSuccessNotification(id === "new" ? "Мера успешно создана" : "Мера успешно обновлена");
                history.push('/centralities');
            } else {
                addErrorNotification();
            }
        })
    }

    const deleteCentrality = () => {
        apiService.deleteCentrality({id: data.id}).then((resp) => {
            if (resp.data.success === "true") {
                addSuccessNotification("Мера успешно удалена");
                history.push('/centralities');
            } else {
                addErrorNotification();
            }
        });
    }

    const addErrorNotification = () => {
        notification.error({
            message: "Упс",
            description: "Что-то пошло не так. Попробуйте еще раз."
        });
    }

    const addSuccessNotification = (text: string) => {
        notification.success({
            message: "Успех!",
            description: text
        });
    }

    return (data?.id || id === "new")
        ? (
        <div className="form">
            <h1>{id === "new" ? "Создание новой меры" : data.name}</h1>
            <Form
                layout='vertical'
                name="basic"
                onFinish={updateCentrality}
            >
                <Form.Item
                    label="Название"
                    name="name"
                    initialValue={data.name || ""}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Описание"
                    name="description"
                    initialValue={data.description || ""}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Год создания"
                    name="year"
                    initialValue={data.year || ""}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Ссылка на описание"
                    name="link"
                    initialValue={data.link || ""}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Ссылка на код"
                    name="codeLink"
                    initialValue={data.codeLink || ""}
                >
                    <Input/>
                </Form.Item>
                <div className="buttons">
                    <Form.Item  >
                        <Button type="primary" htmlType="submit">
                            {id === "new" ? "Создать" : "Сохранить"}
                        </Button>
                    </Form.Item>
                    <Button onClick={() => {
                        history.goBack()
                    }}>
                        {id === "new" ? "Вернуться назад" : "Отменить изменения"}
                    </Button>
                    {id !== "new" && (
                        <Button type="primary" danger onClick={deleteCentrality}>
                            Удалить меру
                        </Button>
                    )}
                </div>
            </Form>
        </div>
        )
        : <div className="spin">
            <Spin size="large"/>
        </div>;
}

export default CentralityPage;
