import React from 'react';

import {Table, Space, notification, Button} from 'antd';
import "antd/lib/table/style/css";


import "./CentralitiesTable.scss";
import ApiService from "../../services/api";
import {Link, useHistory} from "react-router-dom";


const columns = [
    {
        title: 'Название',
        dataIndex: 'name',
        render: (text: string, {id}: any) => {
            const link = `centrality/${id}`;
            return <Link to={link}>{text}</Link>
        }
    },
    {
        title: 'Год создания',
        dataIndex: 'year',
        width: '150px',
        sorter: (a: any, b: any) => a.year - b.year,
    },
    {
        title: 'Описание',
        dataIndex: 'description'
    },
    {
        title: 'Ссылка на описание',
        dataIndex: 'link',
        render: (text: string) => <a href={text} target="_blank">{text}</a>,
    },
    {
        title: 'Ссылка на код',
        dataIndex: 'codeLink',
        render: (text: string) => <a href={text} target="_blank">{text}</a>,
    }
];

const apiService = new ApiService();

export const CentralitiesTable = () => {
    const [data, setData] = React.useState([]);
    const history = useHistory();

    React.useEffect(() => {
        apiService.getCentralityTable().then(({data}) => {
            setData(data.data)
        })
    }, [])

    const createCentrality = () => {
        history.push('/centrality/new');
    }

    const tableProps = {
        bordered: true,
        loading: data.length === 0,
        pagination: true,
        title: undefined,
        showHeader: true,
        scroll: {
            x: 100
        },
        hasData: true,
        tableLayout: undefined,
        top: 'none',
        bottom: 'bottomRight',
    };

    return (
        <div className="table">
            <Button className="add-button" onClick={createCentrality}>
                Добавить новую меру
            </Button>
            <Table
                {...tableProps}
                pagination={{ position: ["topCenter"] }}
                columns={columns}
                dataSource={data}
            />
        </div>
    );
}

export default CentralitiesTable;
