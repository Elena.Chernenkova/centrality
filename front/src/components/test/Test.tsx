import React, {SyntheticEvent} from 'react';

import {Radio, Input, Space, RadioChangeEvent, Button, Image} from 'antd';
import "antd/lib/image/style/css";


import "./Test.scss";
import steps from "./steps.json";

import "./pictures/n4g1.png";
import {useHistory} from "react-router-dom";


export const Test = () => {
    const [currentStep, setCurrentStep] = React.useState("0");
    const [previousStep, setPreviousStep] = React.useState("");
    const history = useHistory();


    const step = steps.filter(item => item.Q === currentStep)[0];
    console.log('step', currentStep, previousStep);

    const onChange = (e: RadioChangeEvent) => {
        setPreviousStep(previousStep + "," + currentStep);
        // @ts-ignore
        switch (e.target.value) {
            case 1:
                setCurrentStep(step.R1 || "");
                break;
            case 2:
                setCurrentStep(step.R2 || "");
                break;
            default:
                setCurrentStep(step.R3 || "");
        }
    };

    const back = ():void => {
        if (previousStep === "") {
            history.push('/');
        } else {
            const ar = previousStep.split(',');
            const newAr = ar.slice(0, -1).join(',');
            setPreviousStep(newAr);
            setCurrentStep(ar[ar.length - 1]);
        }
    }

    const notFinish = () => (
        <div className="not-finish padding">
            <h1>Выберите вариант ответа, который кажется наиболее подходящим</h1>
            <h3>{"Ответ \"1 = 2\" означает, что узлы 1 и 2 имеют одинаковую центральноть; \"2 > 1\" означает, что центральность узла 2 выше"}</h3>
            <div className="flex">
                <Image preview={false} src={"/pictures/" + step.GraphLabel + ".png"}/>
                <Radio.Group onChange={onChange}>
                    <Space direction="vertical">
                        <Radio value={1}>{step.O1}</Radio>
                        <Radio value={2}>{step.O2}</Radio>
                        {step.O3 &&<Radio value={3}>{step.O3}</Radio>}
                    </Space>
                </Radio.Group>
                <Button onClick={back}>Назад</Button>
            </div>
        </div>
    )

    const finish = () => (
        <div className="finish padding">
            <h1>Ваша мера:</h1>
            <h1>{step.MeasureLabel}</h1>
            <h3>({step.MeasureName})</h3>
            <Button onClick={back}>Назад</Button>
        </div>
    )

    return (
        <div className="test">
            {step.Terminal === "1" ? finish() : notFinish()}
        </div>
    );
}

export default Test;
