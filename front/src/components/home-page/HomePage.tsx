import React from 'react';
import { Link } from 'react-router-dom';

import "./HomePage.scss";
import {Button, Form, Input, Spin} from 'antd';
import ApiService from "../../services/api";

const apiService = new ApiService();

export const HomePage = (props: any) => {
    const [isEditor, setEditor] = React.useState(false);
    const [isLoading, setLoading] = React.useState(false);
    const [user, setUser] = React.useState({
        "id": props.id,
        "user": "",
        "role": "",
        "name": "",
        "lastName": ""
    });

    React.useEffect(() => {
        if (!isEditor) {
            setLoading(true);
            apiService.getUserData(props.id).then((resp) => {
                setUser({...user, ...resp.data})
                setLoading(false);
            })
        }
    }, [isEditor])


    const getInfoItem = (label: string, value: string):JSX.Element => (
        <div className="info-item">
            <span className="label">{label}:</span>
            <span>{value || "(пусто)"}</span>
        </div>
    )

    const updateUser = (values: any) => {
        apiService.updateUserData({...values, id: user.id}).then((resp) => {
            if (resp.data.success === "true") {
                setEditor(false);
            }
        });
    }

    const getData = () => {
        return isEditor ? (
            <div>
                <Form
                    layout='vertical'
                    name="basic"
                    onFinish={updateUser}
                >
                    <Form.Item
                        label="Логин"
                        name="user"
                        initialValue={user.user || ""}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        label="Имя"
                        name="name"
                        initialValue={user.name || ""}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        label="Фамилия"
                        name="lastName"
                        initialValue={user.lastName || ""}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item  >
                        <Button type="primary" htmlType="submit">
                            Сохранить
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        ) : (
            <div>
                {getInfoItem("Логин", user.user)}
                {getInfoItem("Уровень доступа", user.role)}
                {getInfoItem("Имя", user.name)}
                {getInfoItem("Фамилия", user.lastName)}
            </div>
        )
    }

    return isLoading ? (
        <div className="spin">
            <Spin size="large"/>
        </div>
        ) : (
        <div className="home-page">
            <h1>Личный кабинет</h1>
            <div className="columns">
                <div className="column" >
                    <h2>Данные пользователя</h2>
                    <Link to="/" >
                        <Button type="link" onClick={() => {setEditor(true)}}>
                            Редактировать
                        </Button>
                    </Link>
                    {getData()}
                </div>
                <div className="column right" >
                    <Link to="/test"><Button type="primary">Пройти тест</Button></Link>
                    <Link to="/centralities"><Button type="primary">Таблица мер</Button></Link>
                    <Button type="primary">Проверка: мера - аксиома</Button>
                </div>
            </div>
        </div>
    );
}

export default HomePage;
