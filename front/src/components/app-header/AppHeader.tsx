import React from 'react';

import { useHistory } from 'react-router-dom'


import "./AppHeader.scss";
import {Button, notification} from 'antd';
import ApiService from "../../services/api";

const apiService = new ApiService();


export const AppHeader = (props: any) => {
    const history = useHistory();

    const getButton = (text: string, onClick: () => void) => {
        return (
            <Button type="primary" onClick={onClick}>
                {text}
            </Button>
        )
    }

    return (
        <div className="header">
            {getButton("Главная", (): void => {
                history.push('/');
            })}
            {getButton("Выход", (): void => {
                apiService.logout().then(function (response) {
                    if (response.data.success === "true") {
                        props.setLogged(false);
                        history.push('/login');
                    } else {
                        notification.error({
                            message: "Не удается выйти",
                            description: "Что-то пошло не так. Попробуйте еще раз."
                        });
                    }
                });
            })}
        </div>
    );
}

export default AppHeader;
