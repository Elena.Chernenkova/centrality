import React from 'react';
import './App.css';
import { Switch, Route, Redirect } from 'react-router-dom';

import LoginPage from "./components/login-page/LoginPage";
import HomePage from "./components/home-page/HomePage";
import AppHeader from "./components/app-header/AppHeader";
import CentralitiesTable from "./components/centralities-table/CentralitiesTable";
import CentralityPage from "./components/centralityPage/CentralityPage";
import Test from "./components/test/Test";


function App() {
    const [isLogged, setLogged] = React.useState({logged: true, id: 6});

    return (
    <div className="app">
        <AppHeader setLogged={setLogged}/>
      <Switch>
        <Route exact path="/login" render={() => <LoginPage setLogged={setLogged}/>} />
        <Protected isLogged={isLogged} path="/test" Component={Test} />
        <Protected isLogged={isLogged} path="/centrality/:id" Component={CentralityPage} />
        <Protected isLogged={isLogged} exact path="/centralities" Component={CentralitiesTable} />
        <Protected isLogged={isLogged} Component={HomePage} />
      </Switch>
    </div>
  );
}

function Protected (props: any) {
    const { isLogged, Component, ...componentProps } = props;
    return <Route {...componentProps} render={(props) => (isLogged.logged
            ? <Component {...props} id={isLogged.id} />
            : <Redirect to='/login' />
    )} />
}

export default App;
