from app import db, login
from flask_login import UserMixin
from .myMixins import CRUDMixin


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.String(100), index=False, unique=True)
    password = db.Column(db.String(100))
    role = db.Column(db.String(10), default='admin')
    name = db.Column(db.String(32), default="")
    lastName = db.Column(db.String(32), default="")

    def __repr__(self):
        return '<User {} - {}>'.format(self.id, self.user)

    def set_password(self, password):
        self.password = password

    def check_password(self, password):
        return self.password == password

    def set_role(self, role):
        self.role = role

    def update(self, kwargs, commit=True):
        for attr, value in kwargs.items():
            setattr(self, attr, value)
        return commit and self.save() or self

    def save(self, commit=True):
        db.session.add(self)
        if commit:
            db.session.commit()
        return self


class Centrality(CRUDMixin, db.Model):
    __tablename__ = 'centralities'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True, index=True, nullable=False)
    year = db.Column(db.String(4), default="0000", nullable=False)
    description = db.Column(db.String(1000), default="")
    link = db.Column(db.String(200))
    codeLink = db.Column(db.String(200))


@login.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
