from flask import Flask
from flask_cors import CORS

from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

app = Flask(__name__)
CORS(app)

app.config.from_object('config.DevelopConfig')
# app.config.from_object('config.ProductionConfig')

db = SQLAlchemy(app)
login = LoginManager(app)
login.login_message = u"Для доступа к данному разделу требуется выполнить вход"
login.login_view = 'login'

from app import routes, models
