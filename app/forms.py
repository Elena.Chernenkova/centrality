from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import ValidationError, DataRequired, EqualTo
from app.models import User


class LoginForm(FlaskForm):
    user = StringField(u'Пользователь', validators=[DataRequired()])
    password = PasswordField(u'Пароль', validators=[DataRequired()])
    submit = SubmitField(u'Вход')


class RegistrationForm(LoginForm):
    user = StringField(u'Пользователь', validators=[DataRequired()])
    password = PasswordField(u'Пароль', validators=[DataRequired()])
    password2 = PasswordField(
        u'Повторите пароль', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField(u'Зарегистрироваться')

    def validate_user(self, user_):
        u = User.query.filter_by(user=user_.data).first()
        if u is not None:
            raise ValidationError( u'Пользователь с данным именем уже зарегистрирован')
