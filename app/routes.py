from flask import render_template, flash, redirect, url_for, request
from flask_login import login_user, logout_user, current_user, login_required
from app import app, db
from app.forms import LoginForm, RegistrationForm
from app.models import User, Centrality
import json


@app.route('/test')
def test():
    return {
        "asd": "asd"
    }


@app.route('/login', methods=['GET', 'POST'])
def login():
    data = request.json["data"]
    user = User.query.filter_by(user=data['user']).first()
    if user is None:
        return {
            "success": "false",
            "error": "Такого пользователя не существует"
        }
    if not user.check_password(data['password']):
        return {
            "success": "false",
            "error": "Неверный пароль"
        }
    return {"success": "true", "id": User.get_id(user)}


@app.route('/register', methods=['POST'])
def register():
    data = request.json["data"]
    user = User(user=data['user'])  # , email=form.email.data)
    user.set_password(data['password'])
    user.set_role('admin')
    db.session.add(user)
    db.session.commit()
    return {"success": "true"}


@app.route('/logout')
def logout():
    logout_user()
    return {"success": "true"}

@app.route('/user/<id>')
def get_user_data(id):
    user = User.query.get(id)
    return {
        "success": "true",
        "user": user.user,
        "role": user.role,
        "name": user.name,
        "lastName": user.lastName
    }


@app.route('/update-user', methods=['POST'])
def update_user_data():
    data = request.json["data"]
    print("aaaaaaaaaaaaaaaaaaaaaa", data)
    user = User.query.get(data["id"])
    user.name = data["name"]
    user.lastName = data["lastName"]
    user.user = data["user"]
    User.save(user)
    return {"success": "true"}


@app.route('/create-centrality', methods=['POST'])
def create_centrality():
    data = request.json["data"]

    new_centrality = Centrality(name=data["name"], year=data["year"], description=data["description"],
                                link=data["link"], codeLink=data["codeLink"])
    db.session.add(new_centrality)
    db.session.commit()
    return {"success": "true"}


@app.route('/update-centrality', methods=['POST'])
def update_centrality():
    data = request.json["data"]
    current = Centrality.get_by_id(data["id"])
    Centrality.update(current, {"name": data["name"], "description": data["description"], "year": data["year"],
                                "link": data["link"], "codeLink": data["codeLink"]})
    return {"success": "true"}


@app.route('/delete-centrality', methods=['POST'])
def delete_centrality():
    current = Centrality.query.get(int(request.json["data"]["id"]))
    Centrality.delete(current)
    return {"success": "true"}


@app.route('/get-centralities', methods=['GET'])
def get_centralities():
    centralities = Centrality.query.all()
    result = []
    for item in centralities:
        result.append({
            "id": item.id,
            "name": item.name,
            "year": item.year,
            "description": item.description,
            "link": item.link,
            "codeLink": item.codeLink
        })
    return {"success": "true", "data": result}


@app.route('/get-centrality/<id>', methods=['GET'])
def get_centrality(id):
    centrality = Centrality.query.get(id)
    print('json', centrality)
    if centrality is None:
        return {"success": "false"}

    result = {
        "id": centrality.id,
        "name": centrality.name,
        "year": centrality.year,
        "description": centrality.description,
        "link": centrality.link,
        "codeLink": centrality.codeLink
    }
    return {"success": "true", "data": result}


@app.route('/initialize-db', methods=['GET'])
def initialize_db():
    db.session.query(Centrality).delete()
    db.session.commit()
    with open('base_centralities.json', encoding='utf-8') as json_file:
        data = json.load(json_file)
        for item in data:
            new_centrality = Centrality(name=item["name"], year=item["year"], description=item["description"],
                                        link=item["link"], codeLink=item["codeLink"])
            db.session.add(new_centrality)
        db.session.commit()
    return {"success": "true"}
# @app.route('/user/:id')
# def logout():
#     logout_user()
#     return {"success": "true"}
